# AutoCADDotNetLibrary.Analyzers

提供`AutoCAD`二次开发项目的[分析器](https://learn.microsoft.com/zh-cn/dotnet/csharp/roslyn-sdk/)。

![](https://img.shields.io/badge/IDE-vs2022-blue)

### 快速开始

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <TargetFramework>net461</TargetFramework>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="AutoCADDotNet" Version="0.8.*" />
    <PackageReference Include="AutoCADDotNetLibrary.Analyzers" Version="0.8.*" />
  </ItemGroup>

</Project>
```

### 效果

![效果](/docs/img/%E6%95%88%E6%9E%9C.png)

### Analyzer

| ID       | 内容                                                 | 严重性 |
| -------- | ---------------------------------------------------- | ------ |
| `AC0001` | 检查`IExtensionApplication`接口是否唯一。            | Error  |
| `AC0002` | 检查`CommandMethodAttribute`所在函数的可访问性。     | Error  |
| `AC0003` | 检查`CommandMethodAttribute`的`globalName`是否重复。 | Error  |

---

### `EnableAutoCADAnalyzers`

为了控制**分析器**的启动，定义`EnableNETAnalyzers`属性。

```xml

<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <TargetFramework>net461</TargetFramework>

    <!-- 是否提供AutoCAD二次开发的分析器，默认值为true。 -->
    <EnableAutoCADAnalyzers>false</EnableAutoCADAnalyzers>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="AutoCADDotNet" Version="0.8.*" />
    <PackageReference Include="AutoCADDotNetLibrary.Analyzers" Version="0.8.*" />
  </ItemGroup>

</Project>
```

## QQ 群

![QQ群](/docs/img/AutoCADDotNetLibrary%E7%BE%A4%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
