﻿using Autodesk.AutoCAD.Runtime;

namespace namespace1
{
    public class Command
    {
        [CommandMethod("cm0")]
        public void A0()
        {
        }

        [CommandMethod(" cm1 ")]
        public void A1()
        {
        }

        [CommandMethod(flags: CommandFlags.Modal, globalName: " Cm1 ")]
        public void A2()
        {
        }

        [CommandMethod("ass", "cm1", CommandFlags.Modal)]
        public void A3()
        {
        }

        [CommandMethod("ass", "cm1", "id", CommandFlags.Modal)]
        public void A4()
        {
        }

        public class Command_copy
        {
            const string cm = "cm1";
            [CommandMethod(cm)]
            public void A1()
            {
            }
        }
    }
}