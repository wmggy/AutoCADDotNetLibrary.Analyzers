﻿using Autodesk.AutoCAD.Runtime;

namespace Test
{
    internal class PublicCommand
    {
        [CommandMethod("PublicCommand1")]
        void M1()
        {

        }

        [CommandMethod("PublicCommand2")]
        void M2()
        {

        }

        void M3()
        {

        }
    }
}
