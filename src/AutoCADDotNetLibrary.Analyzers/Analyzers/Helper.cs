﻿using Microsoft.CodeAnalysis.Diagnostics;

namespace AutoCADDotNetLibrary.Analyzers.Analyzers
{
    public static class Helper
    {
        public static bool ErrorEnableAutoCADAnalyzersValue { get; set; } = false;//使测试可以通过

        public static bool GetEnableAutoCADAnalyzers(AnalyzerOptions options)
        {
            options.AnalyzerConfigOptionsProvider.GlobalOptions.TryGetValue("build_property.EnableAutoCADAnalyzers", out string enableAutoCADAnalyzers);

            if (bool.TryParse(enableAutoCADAnalyzers, out bool result))
            {
                return result;
            }
            return ErrorEnableAutoCADAnalyzersValue;
        }
    }
}
