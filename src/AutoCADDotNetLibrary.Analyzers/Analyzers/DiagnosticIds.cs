﻿using Microsoft.CodeAnalysis;

namespace AutoCADDotNetLibrary.Analyzers.Analyzers
{
    public static class ACDiagnostic
    {
        public static readonly DiagnosticDescriptor Rule_IExtensionApplicationOnlyOne =
           new DiagnosticDescriptor(
               "AC0001",
               "检查IExtensionApplication接口是否唯一",
               "'{0}'类型的继承存在重复。",
               nameof(AutoCADDotNetLibrary.Analyzers),
               DiagnosticSeverity.Error,
               isEnabledByDefault: true);

        public static readonly DiagnosticDescriptor Rule_CommandMethodAttribute_Public =
          new DiagnosticDescriptor(
              "AC0002",
              "检查CommandMethodAttribute所在函数的可访问性",
              "'{0}'的可访问性必须为public。",
              nameof(AutoCADDotNetLibrary.Analyzers),
              DiagnosticSeverity.Error,
              isEnabledByDefault: true);

        public static readonly DiagnosticDescriptor Rule_CommandMethodAttribute_Repeat =
           new DiagnosticDescriptor(
               "AC0003",
               "检查CommandMethodAttribute的globalName是否重复",
               "CommandMethodAttribute的globalName的'{0}'值存在重复。",
               nameof(AutoCADDotNetLibrary.Analyzers),
               DiagnosticSeverity.Error,
               isEnabledByDefault: true);
    }
}
