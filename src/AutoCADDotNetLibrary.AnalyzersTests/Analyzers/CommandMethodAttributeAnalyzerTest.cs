﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Verify = AutoCADDotNetLibrary.AnalyzersTests.Common.CSharpAnalyzerVerifier<AutoCADDotNetLibrary.Analyzers.Analyzers.CommandMethodAttributeAnalyzer>;

namespace AutoCADDotNetLibrary.AnalyzersTests.Analyzers
{
    [TestClass()]
    public class CommandMethodAttributeAnalyzerTest
    {
        [TestMethod()]
        public async Task ExecuteTest()
        {
            string test = @"
using Autodesk.AutoCAD.Runtime;

namespace namespace1
{
    public class Command
    {
        [CommandMethod(""cm0"")]
        public void A0()
        {
        }

        [CommandMethod("" cm1 "")]
        public void A1()
        {
        }

        [CommandMethod(flags: CommandFlags.Modal, globalName: "" Cm1 "")]
        public void A2()
        {
        }

        [CommandMethod(""ass"", ""cm1"", CommandFlags.Modal)]
        public void A3()
        {
        }

        [CommandMethod(""ass"", ""cm1"", ""id"", CommandFlags.Modal)]
        public void A4()
        {
        }

        public class Command_copy
        {
            const string cm = ""cm1"";
            [CommandMethod(cm)]
            public void A1()
            {
            }
        }
    }
}
";
            DiagnosticResult[] expected =
            {
                Verify.Diagnostic().WithSpan(13, 24, 13, 31).WithArguments(" cm1 "),
                Verify.Diagnostic().WithSpan(18, 51, 18, 70).WithArguments(" Cm1 "),
                Verify.Diagnostic().WithSpan(23, 31, 23, 36).WithArguments("cm1"),
                Verify.Diagnostic().WithSpan(28, 31, 28, 36).WithArguments("cm1"),
                Verify.Diagnostic().WithSpan(36, 28, 36, 30).WithArguments("cm1"),
            };

            await Verify.VerifyAnalyzerAsync(test, expected);
        }
    }
}
