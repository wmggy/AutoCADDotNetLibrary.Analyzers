﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Verify = AutoCADDotNetLibrary.AnalyzersTests.Common.CSharpAnalyzerVerifier<AutoCADDotNetLibrary.Analyzers.Analyzers.PublicCommandAnalyzer>;

namespace AutoCADDotNetLibrary.AnalyzersTests.Analyzers
{
    [TestClass()]
    public class PublicCommandAnalyzerTest
    {
        [TestMethod()]
        public async Task ExecuteTest()
        {
            string test = @"
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    internal class PublicCommand
    {
        [CommandMethod(""PublicCommand1"")]
        void M1()
        {

        }

        [CommandMethod(""PublicCommand2"")]
        void M2()
        {

        }

        void M3()
        {

        }
    }
}
";

            DiagnosticResult[] expected =
            {
                Verify.Diagnostic().WithSpan(6, 20, 6, 33).WithArguments("PublicCommand"),
                Verify.Diagnostic().WithSpan(9, 14, 9, 16).WithArguments("M1"),
                Verify.Diagnostic().WithSpan(15, 14, 15, 16).WithArguments("M2"),
            };
            await Verify.VerifyAnalyzerAsync(test, expected);
        }
    }
}
