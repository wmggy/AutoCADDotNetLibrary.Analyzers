﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Verify = AutoCADDotNetLibrary.AnalyzersTests.Common.CSharpAnalyzerVerifier<AutoCADDotNetLibrary.Analyzers.Analyzers.IExtensionApplicationAnalyzer>;

namespace AutoCADDotNetLibrary.AnalyzersTests.Analyzers
{
    [TestClass()]
    public class IExtensionApplicationAnalyzerTest
    {
        [TestMethod()]
        public async Task ExecuteTest()
        {
            string test1 = @"
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class Init1 : IExtensionApplication
    {
        public void Initialize()
        {
        }

        public void Terminate()
        {
        }
    }

    public class Init2 : IExtensionApplication
    {
        public void Initialize()
        {
        }

        public void Terminate()
        {
        }
    }
}
";
            DiagnosticResult[] expected =
            {
                Verify.Diagnostic().WithSpan(6, 18, 6, 23).WithArguments("Test.Init1"),
                Verify.Diagnostic().WithSpan(17, 18, 17, 23).WithArguments("Test.Init2"),
            };
            await Verify.VerifyAnalyzerAsync(test1, expected);


            string test2 = @"
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class Init1 : IExtensionApplication
    {

        public void Initialize()
        {
        }

        public void Terminate()
        {
        }
    }
}
";
            await Verify.VerifyAnalyzerAsync(test2);
        }
    }
}
